﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collection
{
    class Program
    {
        private static int[] intValues = { 1, 2, 3, 4, 5, 6 };
        private static double[] doubleValues = { 8.4, 9.3, 0.2, 7.9, 3.4 };
        private static int[] intValuesCopy;

        static void Main(string[] args)
        {
            //1.Array
            intValuesCopy = new int[intValues.Length];//initialized class method
            Console.WriteLine("Initial array values:\n");
            PrintArrays();

            //sort double values
            Array.Sort(doubleValues);//sort is inbuild static class of array

            //copy values from intValues to intValuesCopy
            Array.Copy(intValues, intValuesCopy , intValues.Length);//copy is static method of Array class

            Console.WriteLine("\nArray values after sort and copy:\n");
            PrintArrays();

            //search 5 in intValues array
            int result = Array.BinarySearch(intValues , 5);

            if (result >= 0)
            {
                Console.WriteLine("5 found at element {0} in intValues" , result);//{0} is a place holder to return the current position
            }
            else
            {
                Console.WriteLine("5 not found in intValues!!");
            }

            //search 8783 in intValues
            int result1 = Array.BinarySearch(intValues, 8783);

            if (result1 >= 0)
            {
                Console.WriteLine("8783 found at element {0} in intValues", result1);//{0} is a place holder to return the current position
            }
            else
            {
                Console.WriteLine("8783 not found in intValues!!");
            }
        }

        private static void PrintArrays()
        {
            Console.WriteLine("doubleValues");
            IEnumerator enumerator = doubleValues.GetEnumerator();
            while (enumerator.MoveNext())
            {
                Console.WriteLine(enumerator.Current+"");
            }

        }
    }
}
