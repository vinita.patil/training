﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryWriterApp
{
    class Program
    {
        static void Main(string[] args)
        {
            //FileStream fs = File.OpenWrite(@"C:\training\products.txt");//create or recreate a file
            //BinaryWriter writer = new BinaryWriter(fs);
            ////product id
            //writer.Write(123);
            ////product name
            //writer.Write("Product 1");
            ////product price
            //writer.Write(34.55);
            ////close writer stream
            //writer.Close();
            //fs.Close();
            //Console.WriteLine("Product details are returned to a file");
            FileStream fs = File.OpenRead(@"C:\training\products.txt");
            BinaryReader reader = new BinaryReader(fs);

            Console.WriteLine("Product ID: " + reader.ReadInt32());
            Console.WriteLine("Product Name: " + reader.ReadString());
            Console.WriteLine("Product Price:" + reader.ReadDouble());


            reader.Close();
            fs.Close();

            Console.ReadLine();
        }
    }
}
