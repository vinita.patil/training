﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Class1
{
    class Program
    {
        static void Main(string[] args)
        {
            DrawPolygon(10, 12);
            DrawPolygon(10, 11, 12, 13);
            
            //int x = 100;
            //Console.WriteLine("In main()()");
            //Console.WriteLine("x before calling chnage() is {0}", x);

            //Change(ref x);
            //Console.WriteLine("In main()()");
            //Console.WriteLine("x before calling chnage() is {0}", x);
            Console.ReadLine();

            ////MyArray ma = new MyArray();
            ////ma.NonStaticMethod();
            //// MyArray.StaticMethod();
            ////Point p = new Point(2,9);
            //IPrinter printer = new LaserPrinter();
            //printer.Print();
            //printer = new LaserPrinter();
            //printer.Print();
            //Console.WriteLine("Printers available are:");
            //Console.WriteLine("1.Laser");
            //Console.WriteLine("2.DotMatrix");
            //Console.WriteLine("Enter your choice");
            //int choice = Convert.ToInt32(Console.ReadLine());
            //IPrinter printer1;

            //switch (choice)
            //{
            //    case 1:printer = new LaserPrinter();
            //        printer.Print();
            //        break;
            //    case 2:printer = new DotMatrixPrinter();
            //        printer.Print();
            //        break;
            //    default: Console.WriteLine("print");
            //        break;

            //}
        }

        //public interface IPrinter
        //{
        // void Print();

        //}
        //public class LaserPrinter : IPrinter
        //{
        //    public void Print()
        //    {
        //       Console.WriteLine("This is laser");
        //    }
        //}
        ////public class DotMatrixPrinter : IPrinter
        ////{
        ////    public void Print()
        ////    {
        ////        Console.WriteLine("This is dm");
        ////    }
        //}
        //static void Change(ref int x)
        //{
        //    Console.WriteLine("In change()");
        //    Console.WriteLine("x before change is {0}", x);

        //    x += 123;
        //    Console.WriteLine("In change()");
        //    Console.WriteLine("x before change is {0}", x);

        //}

        static void DrawPolygon(params int[] points)
        {
            Console.WriteLine("A poly of {0} sides....", points.Length);
        }
    }
}
