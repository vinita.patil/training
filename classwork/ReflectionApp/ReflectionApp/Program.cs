﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ReflectionApp
{
    class Program
    {
        static void Main(string[] args)
        {
            //Type t = typeof(int);
            //Console.WriteLine("Is abstract : {0}" , t.IsAbstract);
            //Console.WriteLine("Is class : {0}" , t.IsClass);
            //Console.WriteLine("Is enum : {0}", t.IsEnum);
            //Console.WriteLine("Is Primitive : {0}", t.IsPrimitive);
            //Console.WriteLine("Is valueType : {0}", t.IsValueType);
            //Console.WriteLine("Is struct : {0}", true);

            //Console.WriteLine("Please enter full path of .dll to be loaded :");
            //String asm = Console.ReadLine();

            ////load the assembly of given path
            //Assembly assembly = Assembly.LoadFile(@asm);
            ////get all types
            //Type[] types = assembly.GetTypes();

            ////enumerate through this collection
            //foreach (Type t in types)
            //{
            //    Console.WriteLine("Type name : {0}" , t.Name);
            //    Console.WriteLine("Is class ? {0}", t.IsClass);
            //    Console.WriteLine();

            //    if (t.IsClass)
            //    {
            //        Console.WriteLine("Methods in this class : ");
            //        foreach (MethodInfo mi in t.GetMethods())
            //        {
            //            Console.WriteLine("Name : {0}" , mi.Name);
            //        }

            //    }

            //}


            Assembly assembly = Assembly.LoadFile(@"C:\training\classwork\ReflectionApp\TripStackLib\bin\Debug\TripStackLib.dll");
            Console.WriteLine("1. Addition");
            Console.WriteLine("2. Subtraction");
            Console.WriteLine("3. Square");
            Console.WriteLine("4. Cube");
            Console.WriteLine("Enter operation nmber :");
            int choice = Convert.ToInt32(Console.ReadLine());

            switch (choice)
            {
                case 1:
                    //load class
                    Type t = assembly.GetType("TripStackLib.ComplexMath");
                    //get default constructor
                    ConstructorInfo constructor = t.GetConstructor(Type.EmptyTypes);
                    //construcct an object
                    object instance = constructor.Invoke(null);
                    //get method to be invoked
                    MethodInfo method = t.GetMethod("Add");

                    Console.WriteLine("Enter first number :");
                    int x = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Enter second number :");
                    int y = Convert.ToInt32(Console.ReadLine());

                    //arrange parameters to be passed to method
                    object[] parameters = { x, y };
                    //invoke method
                    object result = method.Invoke(instance, parameters);
                    //use methods returned data
                    Console.WriteLine("Addition of {0} and {1} is {2}", x , y , result);
                    break;

                case 2:
                    Type t2 = assembly.GetType("TripStackLib.ComplexMath");
                    //get default constructor
                    ConstructorInfo constructor2 = t2.GetConstructor(Type.EmptyTypes);
                    //construcct an object
                    object instance2 = constructor2.Invoke(null);
                    //get method to be invoked
                    MethodInfo method2 = t2.GetMethod("Sub");

                    Console.WriteLine("Enter first number :");
                    int x2 = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Enter second number :");
                    int y2 = Convert.ToInt32(Console.ReadLine());

                    //arrange parameters to be passed to method
                    object[] parameters2 = { x2, y2 };
                    //invoke method
                    object result2 = method2.Invoke(instance2, parameters2);
                    //use methods returned data
                    Console.WriteLine("Subtraction of {0} and {1} is {2}", x2, y2, result2);
                    break;

                case 3:
                    Type t3 = assembly.GetType("TripStackLib.SimpleMath");
                    //get default constructor
                    ConstructorInfo constructor3 = t3.GetConstructor(Type.EmptyTypes);
                    //construcct an object
                    object instance3 = constructor3.Invoke(null);
                    //get method to be invoked
                    MethodInfo method3 = t3.GetMethod("Square");

                    Console.WriteLine("Enter the number :");
                    int x3 = Convert.ToInt32(Console.ReadLine());
                   

                    //arrange parameters to be passed to method
                    object[] parameters3 = { x3};
                    //invoke method
                    object result3 = method3.Invoke(instance3, parameters3);
                    //use methods returned data
                    Console.WriteLine("Square of {0} is {1}", x3, result3);
                    break;

                case 4:
                    Type t4 = assembly.GetType("TripStackLib.SimpleMath");
                    //get default constructor
                    ConstructorInfo constructor4 = t4.GetConstructor(Type.EmptyTypes);
                    //construcct an object
                    object instance4 = constructor4.Invoke(null);
                    //get method to be invoked
                    MethodInfo method4 = t4.GetMethod("Cube");

                    Console.WriteLine("Enter the number :");
                    int x4 = Convert.ToInt32(Console.ReadLine());


                    //arrange parameters to be passed to method
                    object[] parameters4 = { x4 };
                    //invoke method
                    object result4 = method4.Invoke(instance4, parameters4);
                    //use methods returned data
                    Console.WriteLine("Cube of {0} is {1}", x4, result4);
                    break;


                default:
                    break;
            }

            Console.ReadLine();
        }
    }
}
