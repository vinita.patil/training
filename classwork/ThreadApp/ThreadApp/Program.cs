﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ThreadApp
{
    class Program
    {

        public delegate int BinaryOp(int x, int y);
        static void Main(string[] args)
        {
            Console.WriteLine("main is invoked on thread no :{0}", Thread.CurrentThread.ManagedThreadId);
            // Add(3,4);

            BinaryOp o = Add;
            Console.WriteLine("calling main at {0}", DateTime.Now.ToString());

            //int result = o.Invoke(12, 23);
            IAsyncResult iar = o.BeginInvoke(10,12,null,null);


            Console.WriteLine("back to main at {0}", DateTime.Now.ToString());

            Console.WriteLine("doin more work in main");
            //Console.WriteLine("result :"result);

            int result = o.EndInvoke(iar);
            Console.WriteLine("{0} + {1} : {2}", 10, 12, result);
            Console.ReadLine();
        }

        static int Add(int a, int b)
        {
            Console.WriteLine("inside Add() at {0}", DateTime.Now.ToString());
            Console.WriteLine("Add is invoked on thread no :{0}" , Thread.CurrentThread.ManagedThreadId);
            Thread.Sleep(5000);//5 secs

            Console.WriteLine("returning to main at {0}", DateTime.Now.ToString());
            return (a + b);
        }
        
    }
}
