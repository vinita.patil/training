﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace StackTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Stack stack = new Stack();
            bool aBool = true;
            char aChar = '$';
            int aInt = 134;
            string aString = "Hello";

            //adding items into stack using push()

            stack.Push(aBool);
            PrintStack(stack);

            stack.Push(aChar);
            PrintStack(stack);

            stack.Push(aInt);
            PrintStack(stack);

            stack.Push(aString);
            PrintStack(stack);

            Console.WriteLine("First item in stack {0}",stack.Peek());
            PrintStack(stack);

            object o = stack.Pop();
            PrintStack(stack);

            Console.ReadLine();

        }

        private static void PrintStack(Stack stack)
        {
            if (stack.Count == 0)
            {
                Console.WriteLine("Stack is empty");
            }
            else
            {
                Console.WriteLine("Stack is:");
                foreach (var item in stack)
                {
                    Console.Write("{0} ",item);
                }
                Console.WriteLine("");
            }
        }
    }
}
