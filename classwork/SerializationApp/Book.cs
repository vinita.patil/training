﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerializationApp
{
    [Serializable]//attribute which gives info to CLR 
   public class Book
    {
        public int BookID { get; set; }
        public string BookName { get; set; }
        public double BookPrice { get; set; }

        string publisher;

        public void SetPublisher(string publisher)
        {
            this.publisher = publisher;
        }

        public override string ToString()
        {
            return string.Format("ID: {0}\tName:{1}\tPrice:{2}\tPublisher:{3}",BookID,BookName,BookPrice,this.publisher);
        }
    }
}
