﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;


namespace SerializationApp
{
    class Program
    {
        static void Main(string[] args)
        {
            //create book.txt to store all books info
            FileStream fs = File.OpenWrite(@"C:\training\books.txt");//creation of stream
            //Create formatter for storing books in books.txt
            BinaryFormatter bf = new BinaryFormatter();//formatter used

            Book b1 = new Book { BookID = 100, BookName = "Intro to C#", BookPrice = 245 };//creation of object
            b1.SetPublisher("ABC");
            Book b2 = new Book { BookID = 101, BookName = "Pro to C#", BookPrice = 560 };
            b2.SetPublisher("XYZ");
            Book b3 = new Book { BookID = 103, BookName = "C#", BookPrice = 580 };
            b2.SetPublisher("PQR");

            List<Book> books= new List<Book> {b1,b2};//for storing multi objcts

            bf.Serialize(fs,books);//adding to stream
           // bf.Serialize(fs, b2);
           

            fs.Close();
            Console.WriteLine("Book stored in books.txt file");

            fs=File.OpenRead(@"C:\training\books.txt");
            
            List<Book> b = (List<Book>)bf.Deserialize(fs);
            //Book b = (Book)bf.Deserialize(fs);
            foreach (var item in b)
            {
                Console.WriteLine(item);
            }
            fs.Close();

           // Console.WriteLine(b);
            Console.ReadLine();
        }
    }
}
