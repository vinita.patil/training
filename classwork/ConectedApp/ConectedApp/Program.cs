﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace ConectedApp
{
    class Program
    {
        static void Main(string[] args)
        {
            using (SqlConnection conn = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=TrainingDB;Integrated Security=True;Pooling=False"))

            {
                conn.Open();//opened connection with db

                //create required command
                SqlCommand command = new SqlCommand();
                //command.CommandText = "select * from Products";
                //command.CommandText = "select * from Products where Products.Price < 30";

                Console.WriteLine("Enter Product ID ");
                Int16 a = Convert.ToInt16 (Console.ReadLine());
                Console.WriteLine(a);
                command.CommandText = "select * from Products where Id="+a;

                //command.CommandText = "select * from Products where Category = 'soccer'";
                command.CommandType = System.Data.CommandType.Text;
                command.Connection = conn;

                //execute command to get data frm db
                SqlDataReader reader = command.ExecuteReader();//execution strt

                //use data frm reader object
                while (reader.Read())
                {
                    Console.WriteLine(string.Format("Id: {0} \tName: {1} \tDecription: {2} \tCategory: {3} \tPrice: {4}", reader["Id"], reader["Name"], reader["Description"], reader["Category"], reader["Price"]));
                    Console.WriteLine();
                }

                Console.ReadLine();
                reader.Close();//close reader
                conn.Close();//closed connection wid db

            }
        }
    }
}
