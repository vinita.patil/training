﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StreamReadWriteApp
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamWriter writer = new StreamWriter(@"C:\training\data.txt",true);//create file with enable appending
            writer.WriteLine("This is line 1");
            writer.WriteLine("This is line 2");
            writer.WriteLine("This is line 3");


            writer.Close();
            Console.WriteLine("Press a key to read it back :");
            Console.ReadKey();

            StreamReader reader = new StreamReader(@"C:\training\data.txt");

            string line = "";
            while ((line = reader.ReadLine()) != null)
            {
                Console.WriteLine(line);
            }

            reader.Close();
        }
    }
}
