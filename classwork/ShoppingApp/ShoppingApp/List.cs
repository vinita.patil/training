﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;//used to access data ffrom database
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingApp
{
    class List
    {
        public void list()
        {
            using (SqlConnection conn = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=TrainingDB;Integrated Security=True;Pooling=False")) //used for giving the database path

            {
                conn.Open();
               
                Console.WriteLine("List of all products present :");
                SqlCommand command = new SqlCommand();
                command.CommandText = "select * from ShoppingProducts";
                command.CommandType = System.Data.CommandType.Text;
                command.Connection = conn;
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {

                    Console.WriteLine(string.Format("Id: {0} \tName: {1} \tDecription: {2} \tCategory: {3} \tPrice: {4}", reader["Id"], reader["Name"], reader["Description"], reader["Category"], reader["Price"]));
                    Console.WriteLine();
                }

                Console.ReadLine();
                reader.Close();
                conn.Close();
            }
        }

    }
}
