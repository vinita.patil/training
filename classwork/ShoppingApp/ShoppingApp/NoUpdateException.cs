﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingApp
{
    class NoUpdateException : ApplicationException
    {
        public NoUpdateException(string message) : base(message)
        {

        }
        public override string Message => base.Message;
    }
}
