﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;//used to access data ffrom database
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingApp
{
    class Program
    {

        public static int cartCount = 0;
        static void Main(string[] args)
        {
            Console.WriteLine("Shopping Application");

            List l = new List();
            CategoryProducts c = new CategoryProducts();
            Purchase p = new Purchase();
            CartList cl = new CartList();
            Delete d = new Delete();
            Update u = new Update();

            using (SqlConnection conn = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=TrainingDB;Integrated Security=True;Pooling=False")) //used for giving the database path
            {
                conn.Open();//connection for database is opened

                Console.WriteLine("Main Menu :");
                int mainflag = 0;
                while (mainflag != 1)
                {
                    Console.WriteLine("Select your choice");
                    Console.WriteLine("1. View the list of all products");
                    Console.WriteLine("2. View category wise products");
                    Console.WriteLine("3. Purchase product");
                    Console.WriteLine("4. View products present in your cart");
                    Console.WriteLine("5. Delete a product from cart");
                    Console.WriteLine("6. Update a product from cart");
                    Console.WriteLine("0. Exit");


                    Int16 choice = Convert.ToInt16(Console.ReadLine());
                    if (choice == 1)
                    {
                        l.list();
                        Console.WriteLine();
                        
                    }

                    if (choice == 2)
                    {
                        c.category();
                        Console.WriteLine();
                        
                    }

                    if (choice == 3)
                    {
                        p.purchase();
                        Console.WriteLine();
                        
                    }

                    if (choice == 4)
                    {
                        cl.cartList();
                        Console.WriteLine();
                       
                    }

                    if (choice == 5)
                    {
                        d.delete();
                        Console.WriteLine();
                        
                    }
                    if (choice == 6)
                    {
                        u.update();
                        Console.WriteLine();

                        //try
                        //{
                        //    u.update();
                        //    Console.WriteLine();
                        //}
                        //catch (NoUpdateException ibe)
                        //{

                        //    Console.WriteLine(ibe.Message); ;
                        //}
                        

                    }
                    if (choice == 0)
                    {
                        mainflag = 1;
                    }
                }

                Console.ReadLine();
                conn.Close();//connection to database is closed
            }
        }
    }
}
