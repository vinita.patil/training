﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;//used to access data ffrom database
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingApp
{
    class Category
    {
        public void category()
        {
            using (SqlConnection conn = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=TrainingDB;Integrated Security=True;Pooling=False")) //used for giving the database path

            {
                conn.Open();
                SqlCommand command = new SqlCommand();

                Console.WriteLine("Products present in different categories are to be displayed ");
                Console.WriteLine();
                int mainflag = 0;
                while (mainflag != 1)
                {
                    Console.WriteLine("Enter to view products present in required category ");
                    Console.WriteLine("1. Sports");
                    Console.WriteLine("2. Clothing");
                    Console.WriteLine("3. Grocessary");
                    Console.WriteLine("0. Go back to main menu");

                    Int16 choice = Convert.ToInt16(Console.ReadLine());

                    if (choice == 1)
                    {
                        command.CommandText = "select * from ShoppingProducts where category = 'Sports'";
                        command.CommandType = System.Data.CommandType.Text;
                        SqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            Console.WriteLine("List of all Sports products :");
                            Console.WriteLine(string.Format("Id: {0} \tName: {1} \tDecription: {2} \tCategory: {3} \tPrice: {4}", reader["Id"], reader["Name"], reader["Description"], reader["Category"], reader["Price"]));

                        }
                        reader.Close();

                    }
                    else if (choice == 2)
                    {
                        command.CommandText = "select * from ShoppingProducts where category = 'Clothing'";
                        command.CommandType = System.Data.CommandType.Text;
                        SqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            Console.WriteLine("List of all Clothing products :");
                            Console.WriteLine(string.Format("Id: {0} \tName: {1} \tDecription: {2} \tCategory: {3} \tPrice: {4}", reader["Id"], reader["Name"], reader["Description"], reader["Category"], reader["Price"]));

                        }
                        reader.Close();

                    }
                    else if (choice == 3)
                    {
                        command.CommandText = "select * from ShoppingProducts where category = 'Grocessary'";
                        command.CommandType = System.Data.CommandType.Text;
                        SqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            Console.WriteLine("List of all Grocessary products :");
                            Console.WriteLine(string.Format("Id: {0} \tName: {1} \tDecription: {2} \tCategory: {3} \tPrice: {4}", reader["Id"], reader["Name"], reader["Description"], reader["Category"], reader["Price"]));

                        }
                        reader.Close();

                    }
                    else if (choice == 0)
                    {
                        mainflag = 1;
                    }

                }
                Console.ReadLine();
                reader.Close();
                conn.Close();
            }
        }
    }
}

