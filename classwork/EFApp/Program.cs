﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFApp
{
    class Program
    {
        static void Main(string[] args)
        {
            TrainingDBEntities db = new TrainingDBEntities();

            // Product prd = new Product { Name = "Tennis Ball", Description = "For child", Category = "Tennis", Price = 30 };

            var prd = db.Products.Where(p => p.Id == 12).SingleOrDefault();
            //prd.Price += 20;
            //db.Products.Add(prd);
            db.Products.Remove(prd);

            db.SaveChanges();

            Console.WriteLine("done");

            //foreach (var item in db.Products.Where(p =>p.Price >30))
            //{
            //    Console.WriteLine("Id:{0}\tName: {1}\tDescription: {2}\tCategory: {3}\tPrice: {4}",item.Id,item.Name,item.Description,item.Description,item.Price);
            //}

            Console.ReadLine();
        }
    }
}
