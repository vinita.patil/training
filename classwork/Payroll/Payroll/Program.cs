﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payroll
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Payroll software have details of:");
            Console.WriteLine("1.Department");
            Console.WriteLine("2.Employee");
            Console.WriteLine("3.Display");
            Console.WriteLine("Enter your choice");
            int choice = Convert.ToInt32(Console.ReadLine());


            switch (choice)
            {
                case 1:
                    Console.WriteLine("Enter the Department ID");
                    int dept_id = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Enter the Department Name");
                    string Dept_name = Console.ReadLine();
                    Console.WriteLine("Enter the Location");
                    string location = Console.ReadLine();
                    Department dpt = new Department(dept_id, Dept_name, location);
                    Console.WriteLine("Department id is :" + dept_id + "\n Department name is" + Dept_name + "\nLocation is" + location);
                    break;
                case 2:
                    Console.WriteLine("Enter the Employee ID");
                    int emp_id = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Enter the Employee Name");
                    string emp_name = Console.ReadLine();
                    Console.WriteLine("Enter the Designation");
                    string Designation = Console.ReadLine();
                    Console.WriteLine("Enter the basic salary");
                    double basic_salary = Convert.ToDouble(Console.ReadLine());
                    Employee emp = new Employee(emp_id, emp_name, Designation, basic_salary);
                    double hra = emp.HRA_fun();
                    double pf = emp.PF_fun();
                    double gs = emp.GS_fun();
                    double ns = emp.NS_fun();
                    Console.WriteLine("Employee ID :" + emp_id + "\n name:" + emp_name + "\nDesignation:" + Designation + "\n Basic Salary" + basic_salary);
                    Console.WriteLine("HRA :" + hra + "\nProvident Fund:" + pf + "\nGross Salary" + gs + "\nNet Salary" + ns);
                    break;
                default:
                    Console.WriteLine("Please enter either 1 or 2 as your choice...");
                    break;
            } while (choice <= 3)
                Console.ReadLine();
        }
    }
}