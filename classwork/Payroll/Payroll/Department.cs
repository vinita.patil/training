﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payroll
{
    class Department
    {
        public int Id;
        public string Dept_name;
        string Location;
        public Department(int id, string dept_n, string location)
        {
            Id = id;
            Dept_name = dept_n;
            Location = location;
        }
    }
}