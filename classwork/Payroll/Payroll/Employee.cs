﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payroll
{
   public class Employee
    {
        int Emp_Id;
        string Emp_Name;
        string Designation;
        double Basic_salary;
        public double HRA;
        double DA;
        double provident_fund;
        double gross_salary;
        double net_salary;
        double Medical;
        public Employee(int E_I, string E_n, string Dg, double B_Sal)
        {
            Emp_Id = E_I;
            Emp_Name = E_n;
            Designation = Dg;
            Basic_salary = B_Sal;
        }
        public double HRA_fun()
        {
            HRA = (Basic_salary * 8) / 100;
            return HRA;
        }
        public double PF_fun()
        {
            provident_fund = (Basic_salary * 12) / 100;
            return provident_fund;
        }
        public double GS_fun()
        {
            Console.WriteLine("Enter the Medical");
            Medical = Convert.ToDouble(Console.ReadLine());
            gross_salary = Basic_salary + HRA + Medical;
            return gross_salary;
        }
        public double NS_fun()
        {
            double PT;
            Console.WriteLine("Enter the Professional Tax");
            PT = Convert.ToDouble(Console.ReadLine());
            net_salary = gross_salary - (PT - gross_salary);
            return net_salary;
        }
    }
}