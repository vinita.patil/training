﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DisconnectedApp
{
    class Program
    {
        static void Main(string[] args)
        {
            using (SqlConnection conn = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=TrainingDB;Integrated Security=True;Pooling=False"))

            {
                //adapter does not need command
                SqlDataAdapter adapter = new SqlDataAdapter("select * from Products", conn);

                

                DataSet ds = new DataSet();//DataSet is like a db which holds table we get frm query fired,and is blank


                adapter.Fill(ds,"Products");//adapter objct will opn cnnctn and perform fetch data n close cnnctn

                DataRow row = ds.Tables[0].NewRow();
                row["Name"] = "Tennis Ball";
                row["Description"] = "For child";
                row["Category"] = "Tennis";
                row["Price"] = 50;
                //row object is been created

                ds.Tables[0].Rows.Add(row);
                adapter.Update(ds, "Products");


                // Console.WriteLine("Columns in Products");
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Console.WriteLine("Name:{0}\tDescription:{1}\tCategory:{2}\tPrice{3}",
                        ds.Tables[0].Rows[i]["Name"], ds.Tables[0].Rows[i]["Description"],
                        ds.Tables[0].Rows[i]["Category"], ds.Tables[0].Rows[i]["Price"]);
                }


                //Console.WriteLine("Name : {0}" , ds.Tables[0].Rows.[0].["Name"]);
                //Console.WriteLine("Name : {0}\tDescription : {1}\tCategory{2}\t{Price}", ds.Tables[0].Rows[0]["Name"], ds.Tables[0].Rows[0]["Description"], ds.Tables[0].Rows[0]["Category"], ds.Tables[0].Rows[0]["Price"]);

                //foreach (var item in ds.Tables[0].Columns)
                //{
                //    Console.WriteLine(item);
                //}
                Console.ReadLine();

            }
        }
    }
}
