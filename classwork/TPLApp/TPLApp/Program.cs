﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace TPLApp
{
    class Program
    {
        static void Main(string[] args)
        {
            //Below is TPL use
            //Task<int> t1 = new Task<int>(GenerateNumbes);
            //Task<string> t2 = new Task<string>(PrintChars);
            //Task<int> t3 = new Task<int>(PrintArray);

            //t1.Start();
            //t2.Start();
            //t3.Start();



            //Below is thread usage

            ////Thread t1 = new Thread(new ThreadStart(GenerateNumbes));
            ////Thread t2 = new Thread(new ThreadStart(PrintChars));
            ////Thread t3 = new Thread(new ThreadStart(PrintArray));

            ////Console.WriteLine("Threads started at : {0}", DateTime.Now.ToString());



            //t1.Start();
            //t2.Start();
            //t3.Start();

            //t1.Join();
            //t2.Join();
            //t3.Join();

            //Console.WriteLine("Threads stopped at : {0}", DateTime.Now.ToString());

            //Console.WriteLine("Using TPL..........");
            //Console.WriteLine("TPL started at : {0}", DateTime.Now.ToString());
            //Parallel.Invoke(
            //    new Action(GenerateNumbes),
            //     new Action(PrintChars),
            //      new Action(PrintArray)
            //    );
            //Console.WriteLine("TPL ended at : {0}", DateTime.Now.ToString());

            //Below code is called factory pattern
            var t1 = Task<int>.Factory.StartNew(() => GenerateNumbes());
            var t2 = Task<string>.Factory.StartNew(() => PrintChars());
            var t3 = Task<int>.Factory.StartNew(() => PrintArray());

            Console.WriteLine(" Numbers generated till {0}" , t1.Result);

            Console.WriteLine(" OG string is {0}", t2.Result);

            Console.WriteLine(" Array count is {0}", t3.Result);


            Console.ReadLine();

        }

        static int GenerateNumbes()
        {
            int i;
            for ( i = 0; i < 10; i++)
            {
                Console.WriteLine("GenerateNumbes() - Number:{0}", i);
                Thread.Sleep(1000);
            }
            return i;
        }
        static string PrintChars()
        {
            string str = "My name is Vini";
            for (int i = 0; i < str.Length; i++)
            {
                Console.WriteLine("PrintCharacters() - Character: {0}", str[i]);
                Thread.Sleep(1000);

            }
            return str;
        }
        static int PrintArray()
        {
            int[] arr = {1,2,3,4,5,6,7,8,9,10 };
            for (int i = 0; i < arr.Length; i++)
            {
                Console.WriteLine("PrintArray() - Array: {0}", arr[i]);
                Thread.Sleep(1000);

            }
            return arr.Count();
        }
    }
}
