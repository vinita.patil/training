﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqToInMemoObjects
{
    class Customer
    {
        public string CustimerID { get; set; }
        public string City { get; set; }

        public override string ToString()
        {
            return CustimerID + "\t" +City;
        }
    }
}
