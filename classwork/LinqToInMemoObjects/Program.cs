﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace LinqToInMemoObjects
{
    class Program
    {
        static void Main(string[] args)
        {
            NumQuery();
            ObjectQuery();
            XMLQuery();
            Console.ReadLine();
        }



        static void NumQuery()
        {
            var numbers = new int[] { 1, 4, 9, 16, 25, 35 };
            var evenNumbes = from p in numbers
                             where (p % 2) == 0
                             select p;
            Console.WriteLine("Result :");
            foreach (var item in evenNumbes)
            {
                Console.WriteLine(item);
            }
        }

        static IEnumerable<Customer> CreateCustomers()
        {
            //return new List<Customer>
            //{
            //    new Customer{CustimerID = "a",City="Berlin"},
            //    new Customer{CustimerID = "b",City="London"},
            //    new Customer{CustimerID = "c",City="Torino"},
            //    new Customer{CustimerID = "d",City="Berlin"},
            //    new Customer{CustimerID = "e",City="Poland"},

            //};

            return from c in XDocument.Load("Customers.xml").Descendants("Customers").Descendants()
                   select new Customer { City = c.Attribute("City").Value,CustimerID = c.Attribute("CustomerID").Value};


        }

        static void ObjectQuery()
        {
            //var customers = new string[] { };
            var customers = from c in CreateCustomers()
                            where (c.City) == "Berlin"
                             select c;
            
            Console.WriteLine("Result :");
            foreach (var item in customers)
            {
                Console.WriteLine(item);
            }
        }

        static void XMLQuery()
        {
            var doc = XDocument.Load("Customers.xml");
            //Console.WriteLine(doc);

            var result = from c in doc.Descendants("Customer")
                         where c.Attribute("City").Value == "London"
                         select c;
            //Console.WriteLine("Result");
            //foreach (var item in result)
            //{
            //    Console.WriteLine("{0}\t",item);
            //}

            //XElement transformDoc = new XElement("Londoner",
            //                                       from customer in result
            //                                       select new XElement("Contact", new XAttribute("ID", customer.Attribute("CustomerID").Value),
            //                                                                     new XAttribute("Name", customer.Attribute("ContractNAme").Value),
            //                                                                      new XAttribute("City", customer.Attribute("City").Value)));
            //Console.WriteLine("Result:\n{0}", transformDoc);

            XElement transformDoc = new XElement("Products",
                                        new XElement("Product",
                                            new XAttribute("Id", 1),
                                                new XElement("Name", "FootBall"),
                                                new XElement("Price", "235")),
                                        new XElement("Product",
                                            new XAttribute("Id", 2),
                                                new XElement("Name", "Bat"),
                                                new XElement("Price", "1235")));

            Console.WriteLine("Result:\n{0}", transformDoc);

            //transformDoc.Save("Londoner.xml");
            transformDoc.Save("Products.xml");
            Console.WriteLine("Saved");
        }

    }
}
