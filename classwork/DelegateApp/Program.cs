﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegateApp
{
    //As it is frst class it is declared in namespace outside class
    delegate void Compute(int a);//all delegates are bydefault frm System.Multicast.Delegates
    class Program
    {
        static void Main(string[] args)
        {
            Compute c = new Compute(Square);//registered square method with c
            //Compute c = Square;  this is allowd to create a delegate objct
            //var result = c.Invoke(13);//used to invoke the square method that is registrd with delegt objct c
            //Console.WriteLine("result :{0}" , result);
            c += Cube;
            c.Invoke(2);//all the methods registered get invoked
            Console.ReadLine();
        }

        static void Square(int x)//method cn b static or nonstatic
        {
            Console.WriteLine("Square of {0} is {1} ",x, (x * x));
        }
        static void Cube(int y)
        {
            Console.WriteLine ("Cube of {0} is {1} ",y,(y * y * y));
        }
    }
}
