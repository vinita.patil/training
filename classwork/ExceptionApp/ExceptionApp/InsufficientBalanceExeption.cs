﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExceptionApp
{
    class InsufficientBalanceExeption : ApplicationException
    {
        public InsufficientBalanceExeption(string message) : base(message)
        {

        }

        public override string Message => base.Message;
    }
}
