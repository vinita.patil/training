﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExceptionApp
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Account a = new Account(5000);
                Console.WriteLine("current balance : {0}", a.Balance);

                a.Deposite(1000);
                Console.WriteLine("aftr deposite : {0}", a.Balance);

                a.Withdraw(10000);
                Console.WriteLine("aftr withdraw : {0}", a.Balance);
            }
            catch (InsufficientBalanceExeption ibe)
            {

                Console.WriteLine(ibe.Message); ;
            }
        }
    }
}
