﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExceptionApp
{
    class Account
    {
        private int balance;
        public Account(int amount)
        {
            this.balance = amount;
        }

        public void Withdraw(int amnt)
        {
            if (this.balance < amnt)
            {
                //insufficientBalanceException
                throw new InsufficientBalanceExeption("not enought balancce");
            }
            this.balance -= amnt;
        }

        public void Deposite(int amt)
        {
            this.balance += amt;
        }

        public int Balance
        { get { return this.balance; }
            set { this.balance = value; }
        }
    }
}
