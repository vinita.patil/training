﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flight_try
{
    class Program
    {
        static void Main(string[] args)

        {
            List<string> myValues = new List<string>();

            string line = "";

            StreamReader reader = new StreamReader(@"C:\training\Provider1.txt");
            if ((line = reader.ReadLine()) != null)
            {
                string[] fields = line.Split(',');
                using (SqlConnection conn = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=TrainingDB;Integrated Security=True;Pooling=False"))
                {
                    conn.Open();
                    while ((line = reader.ReadLine()) != null)
                    {
                        SqlCommand cmd = new SqlCommand("INSERT INTO Test(Origin, DepartureTime, Destination, DestinationTime, Price) VALUES (@origin, @departureTime, @destination, @destinationTime, @price)", conn);
                        //cmd.Parameters.AddWithValue("@id", fields[0].ToString());
                        cmd.Parameters.AddWithValue("@origin", fields[0].ToString());
                        cmd.Parameters.AddWithValue("@departureTime", fields[1].ToString());
                        cmd.Parameters.AddWithValue("@destination", fields[2].ToString());
                        cmd.Parameters.AddWithValue("@destinationTime", fields[3].ToString());
                        cmd.Parameters.AddWithValue("@price", fields[4].ToString());
                        cmd.ExecuteNonQuery();



                    }
                    conn.Close();
                }


            }
        }
    }
}
