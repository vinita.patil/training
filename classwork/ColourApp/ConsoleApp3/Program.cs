﻿using FNLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            FlightColours fc = new FlightColours { ID = Convert.ToInt32(args[0]), Name = args[1]};
            fc.PrintColor();
            
        }
    }
}
