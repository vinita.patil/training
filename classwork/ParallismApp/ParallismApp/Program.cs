﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ParallismApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("using c# for loop {0}",DateTime.Now.ToString());
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("i = {0}, Thread = {1}", i , Thread.CurrentThread.ManagedThreadId);
                Thread.Sleep(100);
            }

            Console.WriteLine("using c# 4.0 parallel.for loop {0}", DateTime.Now.ToString());
            Parallel.For(0, 10, i => {

                Console.WriteLine("i = {0}, Thread = {1}", i, Thread.CurrentThread.ManagedThreadId);
                Thread.Sleep(100);

            });


            Console.ReadLine();
        }
    }
}
