﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FileIOApp
{
    class Program
    {
        static void Main(string[] args)
        {
            DirectoryInfo dinfo = new DirectoryInfo(@"C:\Windows");// "\" stands for excape so we use \\, @ sign can be used instd
            //Console.WriteLine("Listing contents of : {0} ", dinfo.FullName);
            //foreach (FileInfo file in dinfo.GetFiles())
            //{
            //    Console.WriteLine("File Name: "+ file.Name);
            //    Console.WriteLine("File Size in (bytes): " + file.Length);
            //    Console.WriteLine("Creation time : " +file.CreationTime);
            //    Console.WriteLine();
            //}
            foreach (DirectoryInfo folder in dinfo.GetDirectories())
            {
                Console.WriteLine("Folder Name: " + folder.Name);
                foreach (var file in folder.GetFiles())
                {
                    Console.WriteLine("File Name: " + file.Name);
                }
            }
            Console.ReadLine();
        }
    }
}
