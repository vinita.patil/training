﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Text.RegularExpressions;

namespace HashTableText
{
    class Program
    {
        static void Main(string[] args)
        {
            //Hashtable hashtable = new Hashtable();
            Hashtable hashtable = CollectWords();
            DisplayHashtable(hashtable);
            Console.ReadLine();

        }

        private static void DisplayHashtable(Hashtable hashtable)
        {
            Console.WriteLine("\nHashtable contains:\n{0,-12}{1,-12}", "Key:", "Value:");
            foreach (var key in hashtable.Keys)
            {
                Console.WriteLine("{0,-12}{1,-12}",key , hashtable[key]);
            }
            Console.WriteLine("\nSize: {0}", hashtable.Count);
        }

        private static Hashtable CollectWords()//helper method
        {
            Hashtable table = new Hashtable();
            Console.WriteLine("Enter a string");
            string input = Console.ReadLine();

            string[] words = Regex.Split(input, @"\s+");//+ indicates 1 or more
            foreach (var word in words)
            {
                string wordKey = word.ToLower();
                if (table.ContainsKey(wordKey))
                {
                    table[wordKey] = ((int)table[wordKey]) + 1;
                }
                else
                {
                    table.Add(wordKey, 1);
                }
            }
            return table;
        }
    }
}
