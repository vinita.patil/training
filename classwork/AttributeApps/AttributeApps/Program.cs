﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttributeApps
{
    class Program
    {
        static void Main(string[] args)
        {
            MyDetails md = new MyDetails { FirstName = "Vini", LastName = "Patil" };
            Printer.Print(md);

            Console.ReadLine();
        }
    }
}
