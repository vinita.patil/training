﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttributeApps
{
    //this attribute can be applied only to a class
    //AllowMultiple means this attribute can be applied once on a class
    //Inherited means when a class inherits a class on which this attibute 
    //is applied the inherited class will not have this attribute in effect

        [AttributeUsage(AttributeTargets.Class,AllowMultiple =false,Inherited =false)]
    class PrintAttribute : Attribute
    {
        string destination;

        public PrintAttribute(string dest)
        {
            this.destination = dest;
        }

        public string Destination
        {
            get
            {
                return this.destination;
            }

        }
    }
}
