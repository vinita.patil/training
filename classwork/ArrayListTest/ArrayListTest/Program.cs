﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;//To use ArrayList

namespace ArrayListTest
{
    class Program
    {
        private static readonly string[] colors = { "MAGENTA", "RED", "WHITE", "BLUE", "CYAN" };
        private static readonly string[] removeColors = {"RED","WHITE","BLUE"};

        static void Main(string[] args)
        {
            ArrayList list = new ArrayList();//here if we give capacity then only that number of elemnts are accptd but if nothing is given then any numbr cn be added\
            //add elements from static array into this list
            foreach (var color in colors)//var is used to select datatype on own, in this case datatype is string
            {
                list.Add(color);

            }

            ArrayList removeList = new ArrayList(removeColors);
            DisplayInfo(list);

            removeColours(list, removeList);
            Console.WriteLine("\nArrayList afte removing colors");
            DisplayInfo(list);

            Console.ReadLine();
        }

        private static void removeColours(ArrayList list, ArrayList removeList)
        {
            //throw new NotImplementedException();
            for (int count = 0; count < removeList.Count; count++)
            {
                list.Remove(removeList[count]);
            }
        }

        private static void DisplayInfo(ArrayList list)
        {
            //throw new NotImplementedException();//it is used for code developers
            //iterate through list and we dont use enumerator because ArrayList has IEnumerable implemented
            foreach (var item in list)
            {
                Console.Write("{0} ",item);
            }
            Console.WriteLine("\nSize: {0}; Capacity {1}", list.Count,list.Capacity);//the capacity initially set is increasing itself with order of 2 i.e 1-2-4-8-16/
            list.Add("BLACK");
            list.Add("ORANGE");
            list.Add("PINK");
            list.Add("PINK");
            Console.WriteLine("\nSize: {0}; Capacity {1}", list.Count, list.Capacity);

            int index = list.IndexOf("BLUE");//boxing 
            if (index != -1)
            {
                Console.WriteLine("The array list contains BLUE at index {0}", index);
            }
            else
            {
                Console.WriteLine("The array list does not contain BLUE anywhere");
            }
        }
    }
}
