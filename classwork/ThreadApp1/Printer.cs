﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadApp1
{
    public class Printer
    {
        char ch;
        int sleepTime;
        string objName;


        //public Printer(char c, int t, string n)
        //{
        //    this.ch = c;
        //    this.sleepTime = t;
        //    this.objName = n;
        //}



        public void Print(object message)
        {

            //Console.WriteLine("Print() of {0} is on thread no: {1}", this.objName, Thread.CurrentThread.ManagedThreadId);

            //for (int i = 0; i < 100; i++)
            //{
            //    Console.WriteLine(this.ch);
            //    Thread.Sleep(this.sleepTime);
            //}

            //-----------04-07-2018 ------

            // acquire a lock
            Monitor.Enter(this);
            try
            {
                Console.Write("*** " + message);
                Thread.Sleep(5000);
                Console.WriteLine(" ***");
            }
            catch (Exception)
            {
                // exception handling code
            }
            finally{
                Monitor.Exit(this);
            }
        }
    }
} 
