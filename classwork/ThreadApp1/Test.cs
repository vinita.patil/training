﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadApp1
{
    public class Test
    {
        public static void DoSomething()
        {
            try
            {
                try
                {
                    try
                    {

                    }
                    catch (ThreadAbortException)
                    {
                        Console.WriteLine("------inner aborted................");
                    }
                }
                catch (ThreadAbortException)
                {
                    Console.WriteLine("-----outer aborted............");
                }
            }
            finally
            {
                Console.WriteLine("---finally...............");
            }
        }
    }
}
