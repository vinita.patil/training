using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadApp2
{
    class TicTok
    {
        public void Tick(bool running)
        {
            lock (this)
            {
                if (running)
                {
                    //notify any waiting threads

                    Monitor.Pulse(this);
                    return;
                }
                Console.WriteLine("Tick");

				//let Tock() to run
                Monitor.Pulse(this);//pulse keyword is used to notify

                //wait Tock() to complete or notify
                Monitor.Wait(this);


            }

        }
        public void Tock(bool running)
        {

            lock (this)
            {
                if (running)
                {
                    //notify any waiting threads

                    Monitor.Pulse(this);//
                    return;
                }
                Console.WriteLine("Tock");

                //let Tick() to run
                Monitor.Pulse(this);//pulse keyword is used to notify

                //wait Tick() to complete or notify
                Monitor.Wait(this);//monitor raises a notification for this object to wait until other method notifies lock release


            }

        }
    }
}