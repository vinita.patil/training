﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("Name on thread no :{0}" , Thread.CurrentThread.ManagedThreadId);

            //Printer p1 = new Printer('.',10,"p1");
            //Printer p2 = new Printer('*',50,"p2");

            //Thread t1 = new Thread(new ThreadStart(p1.Print));
            //Thread t2 = new Thread(new ThreadStart(p2.Print));
            //Console.WriteLine("t1 priority:{0}",t1.Priority);

            //Console.WriteLine("t2 priority:{0}", t2.Priority);

            //t1.Start();
            //t2.Start();

            //Console.WriteLine("Main() waiting");
            //t1.Join();
            //t2.Join();

            //Console.WriteLine("Main() completed");



            /*
            Thread t = new Thread(new ThreadStart(Test.DoSomething));
            t.Start();
            Thread.Sleep(10);
            t.Abort();
            //t.Start();
            t.Join();

            Console.WriteLine("done");
            

            Printer p = new Printer();

            ParameterizedThreadStart ps = p.Print;//creation of overloaded , used to pass parameters to method

            Thread t1 = new Thread(ps);
            Thread t2 = new Thread(ps);
            Thread t3 = new Thread(ps);

            t1.Start("welcome");
            t2.Start("To");
            t3.Start("Tripstack");

            t1.Join();
            t2.Join();
            t3.Join();
            */

           

            //Console.ReadLine;

        }
    }
    }
