﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NonConnectedApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter following info");
            Console.WriteLine("Name");
            string name = Console.ReadLine();
            Console.WriteLine("Decreiption");
            string description = Console.ReadLine();
            Console.WriteLine("Category");
            string category = Console.ReadLine();
            Console.WriteLine("Price");
            double price = Convert.ToDouble(Console.ReadLine());

            string insertCommand = string.Format("insert into Products (Name,Description,Category,Price) values ('{0}','{1}','{2}','{3}')", name, description, category, price);

            using (SqlConnection conn = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=TrainingDB;Integrated Security=True;Pooling=False"))

            {
                conn.Open();

                SqlCommand command = new SqlCommand();
                command.CommandText = insertCommand;
                command.CommandType = System.Data.CommandType.Text;
                command.Connection = conn;

                var result = command.ExecuteNonQuery();
                if (result > 0)
                {
                    Console.WriteLine("Success");
                }
                else
                {
                    Console.WriteLine("Failed");
                }


                Console.ReadLine();
                
                conn.Close();
            }
        }
    }
}
