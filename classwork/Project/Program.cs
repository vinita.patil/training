﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Project
{
    class Program
    {
        static List<Department> departments;
        static List<Employee> employees;

        //static int deptID = 0;
        static int empID = 0;

        static void Main(string[] args)
        {
            
            
            departments = new List<Department>();
            employees = new List<Employee>();
            int mainflag = 0;
            while (mainflag != 1)
            {


               
                Console.WriteLine("1. Create");
                Console.WriteLine("2. Read All");
                Console.WriteLine("3. Read Specific");
                Console.WriteLine("4. Update");
                Console.WriteLine("5. Delete");
                Console.WriteLine("0. Exit");
                int c1 = Convert.ToInt32(Console.ReadLine());
                if (c1 == 1)
                {
                    Console.WriteLine("Enter 1 to goto Department and 2 to goto Employee");
                    int choice = Convert.ToInt32(Console.ReadLine());
                    if (choice == 1)
                    {
                        Department d = new Department();
                        Console.WriteLine("Enter Department ID");
                        d.deptID = Convert.ToInt32(Console.ReadLine());
                        int flag = 0;
                        foreach (var item in departments)
                        {
                           
                            if (item.deptID == d.deptID)
                            {
                                Console.WriteLine("Department is present");
                                flag = 1;
                            }

                        }
                        if (flag == 0)
                        {


                            Console.WriteLine("Enter Department_Name");
                            d.deptName = Console.ReadLine();
                            Console.WriteLine("Enter Location");
                            d.Location = Console.ReadLine();
                            departments.Add(d);
                        }
                    }
                    if (choice == 2)
                    {
                        Employee e = new Employee();
                        empID = empID + 1;
                        e.empID = empID;
                        Console.WriteLine("Enter Name");
                        e.empName = Console.ReadLine();
                        Console.WriteLine("Enter the option of Designation as below");
                        Console.WriteLine("1. Employee");
                        Console.WriteLine("2. Manager");
                        Console.WriteLine("3. TL");
                        int ch = Convert.ToInt32(Console.ReadLine());
                        if (ch == 1)
                        {
                            e.Designation = "Employee";
                        }
                        if (ch == 2)
                        {
                            e.Designation = "Manager";
                        }
                        if (ch == 3)
                        {
                            e.Designation = "TL";
                        }
                        Console.WriteLine("Enter your Basic Salary");
                        e.BasicSalary = Convert.ToInt32(Console.ReadLine());
                        e.hra = (8 * e.BasicSalary) / 100;
                        e.pf = (12 * e.BasicSalary) / 100;
                        e.gs = e.BasicSalary + e.hra;
                        e.ns = e.gs - (e.hra + e.pf);
                        Console.WriteLine("Enter the DepartmentID");
                        e.depID = Convert.ToInt32(Console.ReadLine());
                        employees.Add(e);

                    }
                }
                if (c1 == 2)
                {
                    Console.WriteLine("Press 1 to Show all departments and 2 to show all employees");
                    int c2 = Convert.ToInt32(Console.ReadLine());
                    if (c2 == 1)
                    {
                        if (departments.Count == 0)
                        {
                            Console.WriteLine("No department");
                        }
                        else
                        {
                            foreach (var item in departments)
                            {
                                Console.WriteLine(item);
                            }
                            
                        }
                    }
                    if (c2 == 2)
                    {
                        if (employees.Count == 0)
                        {
                            Console.WriteLine("No employee information");
                        }
                        else
                        {
                            foreach (var item in employees)
                            {
                                Console.WriteLine(item);
                            }
                            
                        }
                    }
                }
                if (c1 == 3)
                {
                    Console.WriteLine("Press 1 if want to know about department and 2 to know about employee");
                    int c3 = Convert.ToInt32(Console.ReadLine());
                    if (c3 == 1)
                    {
                        Console.WriteLine("Enter name of the Department ID to be searched");
                        int did = Convert.ToInt32(Console.ReadLine());
                        int ind = -1;
                        int flg = 0;
                        foreach (var item in departments)
                        {
                            if (departments.Count == 0)
                            {
                                Console.WriteLine("Department doesnt have any entry!!!");
                            }

                            ind += 1;
                            if (item.deptID == did)
                            {
                                Console.WriteLine(departments[ind]);
                                flg = 1;
                                break;
                            }
                        }
                        if (flg == 0)
                        {
                            Console.WriteLine("Given Department ID not present in the list!!!!");
                        }
                    }
                    if (c3 == 2)
                    {
                        Console.WriteLine("Enter the employee name to be searched");
                        string opt = Console.ReadLine();
                        if (employees.Count == 0)
                        {
                            Console.WriteLine("Employee doesnt contain any information");
                        }
                        int ind = -1;
                        foreach (var item in employees)
                        {
                            ind += 1;
                            if (item.empName == opt)
                            {
                                Console.WriteLine(employees[ind]);
                            }
                        }
                    }
                }
                if (c1 == 4)
                {
                    Console.WriteLine("Enter Employee Name to be updated");
                    string opt = Console.ReadLine();
                    int ind = -1;
                    foreach (var item in employees)
                    {
                        ind += 1;
                        if (item.empName == opt)
                        {
                            int flag = 0;
                            while (flag != 1)
                            {
                                Console.WriteLine("Enter what you need to update");
                                Console.WriteLine("1. Employee Name");
                                Console.WriteLine("2. Designation");
                                Console.WriteLine("3. Basic Salary");
                                Console.WriteLine("4. Department ID");
                                Console.WriteLine("0. If dont want to update anything");
                                int ch4 = Convert.ToInt32(Console.ReadLine());
                                if (ch4 == 1)
                                {
                                    Console.WriteLine("Enter ID of employee you want to change");
                                    int val = Convert.ToInt32( Console.ReadLine());
                                    employees[ind].empID =  val;
                                }
                                if (ch4 == 2)
                                {
                                    Console.WriteLine("Enter the option of Designation as below");
                                    Console.WriteLine("1. Employee");
                                    Console.WriteLine("2. Manager");
                                    Console.WriteLine("3. TL");
                                    int ch = Convert.ToInt32(Console.ReadLine());
                                    if (ch == 1)
                                    {
                                        employees[ind].Designation = "Employee";
                                    }
                                    if (ch == 2)
                                    {
                                        employees[ind].Designation = "Manager";
                                    }
                                    if (ch == 3)
                                    {
                                        employees[ind].Designation = "TL";
                                    }
                                }
                                if (ch4 == 3)
                                {
                                    Console.WriteLine("Enter your change in salary");
                                    int newsal = Convert.ToInt32(Console.ReadLine());
                                    double hra2 = (8 * newsal) / 100;
                                    double pf2 = (12 * newsal) / 100;
                                    double gs2 = newsal + hra2;
                                    double ns2 = gs2 - (hra2 + pf2);
                                    employees[ind].BasicSalary = newsal;
                                    employees[ind].hra = hra2;
                                    employees[ind].pf = pf2;
                                    employees[ind].gs = gs2;
                                    employees[ind].ns = ns2;

                                }
                                if (ch4 == 4)
                                {
                                    Console.WriteLine("Enter the changed Department ID");
                                    int depid = Convert.ToInt32(Console.ReadLine());
                                    employees[ind].depID = depid;
                                }
                                if (ch4 == 0)
                                {
                                    flag = 1;

                                }
                            }
                        }
                    }
                }
                if (c1 == 5)
                {
                    Console.WriteLine("Press 1 to delete department information and 2 to delete employee information");
                    int c5 = Convert.ToInt32(Console.ReadLine());
                    if (c5 == 1)
                    {
                        Console.WriteLine("Enter the department ID you want to delete");
                        int o = Convert.ToInt32(Console.ReadLine());
                        int flag = 0;
                        foreach (var item in employees)
                        {
                            if (item.depID == o)
                            {
                                Console.WriteLine("Sorry cannot delete the information");
                                flag = 1;
                            }
                        }
                        if (flag == 0)
                        {
                            int newflag = 0;
                            int newind = -1;
                            foreach (var item in departments)
                            {

                                newind += 1;
                                if (item.deptID == o)
                                {
                                    departments.RemoveAt(newind);
                                    newflag = 1;
                                    break;
                                }
                            }
                            if (newflag == 0)
                            {
                                Console.WriteLine("Department ID not present");
                            }
                        }
                    }
                    if (c5 == 2)
                    {
                        Console.WriteLine("Enter the employee name you want to delete");
                        string name = Console.ReadLine();
                        int newnewflag = 0;
                        int newnewind = -1;
                        foreach (var item in employees)
                        {
                            newnewind += 1;
                            if (item.empName == name)
                            {
                                employees.RemoveAt(newnewind);
                                newnewflag = 1;
                            }

                        }
                        if (newnewflag == 0)
                        {
                            Console.WriteLine("Sorry Name not found");
                        }
                    }

                }
                if (c1 == 0)
                {
                    mainflag = 1;
                }
            }

        }
    }
}
