﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Project
{
    class Employee
    {
        public int empID { get; set; }
        public string empName { get; set; }
        public string Designation { get; set; }
        public double BasicSalary { get; set; }
        public double hra { get; set; }
        public double pf { get; set; }
        public double gs { get; set; }
        public double ns { get; set; }
        public int depID { get; set; }

        public override string ToString()
        {
            
            return string.Format("Employee ID: {0}\nEmployee Name: {1}\nDesignation: {2}\nBasic Salary: {3}\nHRA: {4}\nPF: {5}\nGS: {6}\nNS: {7}\nDepartment ID: {8}", empID, empName, Designation, BasicSalary, hra, pf, gs, ns, depID);
        }
    }
}
