﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project
{
     public class Department
    {
        public int deptID { get; set; }
        public string deptName { get; set; }
        public string Location { get; set; }

        public override string ToString()
        {
            return string.Format("ID: {0}\nName: {1}\nLocation: {2}", deptID, deptName, Location);
        }
        public int EmpCount { get; set; }
    }
    
}

