﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Login
{
   //public delegate void LoginSuccessfull(string a);
   //public delegate void LoginFail(string a);

    class Program
    {
        static void Main(string[] args)
        {

            Login l1 = new Login();
            Console.WriteLine("Enter the username");
            string userName = Console.ReadLine();
            Console.WriteLine("Enter the Password");
            string password = Console.ReadLine();
            l1.LoginSuccessfull += OnSucess;
            l1.LoginFail += OnFailed;
            User u1 = new User();
            var result = l1.Authenticate(u1);
            l1.LoginResult(u1, result);

        }

        private static void OnFailed(Args args)
        {
            Console.WriteLine("Hello {0}Login is Sucessfull with {1} password!!!", args.userName, args.password);
        }

        private static void OnSucess(Args args)
        {

            Console.WriteLine("Hello {0}Login is Failed with {1} password!!!", args.userName, args.password);
        }
    }

    class Args
    {
        public String userName { get; set; }
        public String password { get; set; }
    }


}

