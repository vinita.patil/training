﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Login
{
    class LoginEventArgs
    {
        public string userName { get; set; }
        public string password { get; set; }
    }
}
