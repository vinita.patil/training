﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegateAnonymous
{
    class Program
    {
        static void Main(string[] args)
        {
            MyButton button = new MyButton();
            button.Click += delegate ()//this is the anonymous method
            {
                Console.WriteLine("MyButton clicked");
            };
            button.RaiseEvent();
        }
    }
    delegate void ClickHandler();
    class MyButton
    {
        public event ClickHandler Click;
        public void RaiseEvent()
        {
            Click();//raising the click event
        }
    }
}
