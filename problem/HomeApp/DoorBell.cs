﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeApp
{
    public delegate void RingBellHandler(string cabinName);

    class DoorBell
    {
        public event RingBellHandler RingBell;
        public void RingTheBell()
        {
            RingBell("Training Room");
        }

    }


}
