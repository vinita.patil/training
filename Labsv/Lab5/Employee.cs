﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    public class Employee
    {
        int emp_id, dept_id;

        string emp_name;
        static int count;
        string emp_designation;
        double basic_salary;

        public Employee(string Ename, string Edesignation, double Bsalary)
        {
            count++;
            emp_id = count;
            emp_name = Ename;
            emp_designation = Edesignation;
            basic_salary = Bsalary;
            
        }

        public override string ToString()
        {
            return emp_name + " "+emp_id+" "+emp_designation+ " "+basic_salary;
        }

        

    }

}
