﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    public class Employee
    {
        int emp_id, dept_id;
        
        string emp_name;
        string emp_designation;
        double basic_salary, hra, da, provident_fund, gross_salary, net_salary, pt;
        

        public Employee(int Eid, string Ename, string Edesignation, double Bsalary)
        {
            
            emp_id = Eid;
            emp_name = Ename;
            emp_designation = Edesignation;
            basic_salary = Bsalary;
            hra = (8* basic_salary)/100;
            da = 1000;
            provident_fund = (12 * basic_salary)/1000;
            gross_salary = basic_salary + hra;
            pt = 1500;
            net_salary = gross_salary - (provident_fund + pt);

        }

        public void display()
        {
            Console.WriteLine("Employee Details....");
            Console.WriteLine("ID: "+emp_id);
            Console.WriteLine("Name: "+emp_name);
            Console.WriteLine("Designation: "+emp_designation);
            Console.WriteLine("Salary: "+basic_salary);
            Console.WriteLine("HRA: "+hra);
            Console.WriteLine("DA: "+da);
            Console.WriteLine("Provident Fund: "+provident_fund);
            Console.WriteLine("Gross Salary: "+gross_salary);
            Console.WriteLine("Net Salary: "+net_salary);
            

        }

             
    }
       
}
