﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    class Program
    {
        static void Main(string[] args)
        {
            string input;
            do
            {
                
                Console.WriteLine("1.Employee Details. \n 2.Department Details.\n Press 1/2 to enter your choice: ");
                int choice = Convert.ToInt32(Console.ReadLine());

                switch (choice)
                {
                    case 1:
                        Console.WriteLine("Enter Employee details as ID, Name, Designation,Salary");
                        string id1 = Console.ReadLine();
                        int id2 = Convert.ToInt32(id1);
                        string name = Console.ReadLine();
                        string designation = Console.ReadLine();
                        String sal1 = Console.ReadLine();
                        int sal2 = Convert.ToInt32(sal1);
                        Employee e = new Employee(id2,name,designation,sal2 );
                        e.display();

                        break;
                    case 2:
                        Console.WriteLine("Enter Department details as ID, Name, Location");
                        string id3 = Console.ReadLine();
                        int id4 = Convert.ToInt32(id3);
                        string dept_name = Console.ReadLine();
                        string dept_location = Console.ReadLine();
                        Department d1 = new Department(id4,dept_name,dept_location);
                        d1.display();
                        break;
                    default:
                        break;
                }


                Console.WriteLine("Do you wish to continue ?(y/n)");
                input=Console.ReadLine();

            } while (input == "y");
        }   
    }
}
