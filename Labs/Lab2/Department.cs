﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    class Department
    {
        int dept_id;
        string dept_name;
        string location;

        public Department(int Did, string Dname, string Dlocation)
        {
            dept_id = Did;
            dept_name = Dname;
            location = Dlocation;
        }

        public void display()
        {
            Console.WriteLine("Department Details......");
            Console.WriteLine("ID: \n"+dept_id+"Name: \n"+dept_name+"Location: \n"+location);
        }
    }
}
